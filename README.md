# Telegram bot Example

• Содержит файлы для развертывания на Heroku

• Решение завернуто в docker-образ

• Реализовано сохранение истории переписки в БД и сбор базовой статистики 

• Настроен CI/CD pipline 


## Run

```
docker-compose up --build
```

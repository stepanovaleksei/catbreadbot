import logging
import os

from aiogram import Bot, Dispatcher, executor, types
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup

from bot.db_querries import Queries
from bot.config import bot_token, db_name

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

db_path = os.path.join(BASE_DIR, f"db/{db_name}")

storage = MemoryStorage()

logging.basicConfig(
    filename="app.log",
    filemode="w",
    format="%(asctime)s - %(message)s",
    datefmt="%d-%b-%y %H:%M:%S",
    level=logging.INFO,
)

bot = Bot(token=bot_token)

dp = Dispatcher(bot, storage=storage)

db = Queries(db_path)


class Form(StatesGroup):
    first_question = State()
    first_answer_yes = State()
    first_answer_no = State()
    second_question = State()


positive_answers = ["конечно", "ага", " пожалуй", "да", "есть", "угу"]
negative_answers = ["нет", "нет, конечно", "ноуп", "найн", "не"]


@dp.message_handler(commands=["start"])
async def first_question_func(message: types.Message):
    if not db.account_exist(message.from_user.id):
        db.add_user(message.from_user.id)
    await message.answer(
        f"Привет, {message.from_user.first_name} {message.from_user.last_name}!\n"
        "Я помогу отличить кота от хлеба. Объект перед тобой квадратный?",
        reply=False,
    )
    await Form.first_question.set()


@dp.message_handler(state=Form.first_question)
async def second_question_func(message: types.Message, state: FSMContext):
    db.add_message(message.from_user.id, message.text)
    if message.text.lower() in positive_answers:
        await message.answer(f"У него есть уши?", reply=False)
        await Form.first_answer_yes.set()

    elif message.text.lower() in negative_answers:
        await message.answer(f"Это кот, а не хлеб! Не ешь его!", reply=False)
        await state.finish()
    else:
        await state.finish()


@dp.message_handler(state=Form.first_answer_yes)
async def answer_func(message: types.Message, state: FSMContext):
    db.add_message(message.from_user.id, message.text)
    if message.text.lower() in positive_answers:
        await message.answer(f"Это кот, а не хлеб! Не ешь его!", reply=False)
        await state.finish()
    elif message.text.lower() in negative_answers:
        await message.answer(f"Это хлеб, а не кот! Ешь его!", reply=False)
        await state.finish()
    else:
        await state.finish()


if __name__ == "__main__":
    try:
        executor.start_polling(dp, skip_updates=True)
    except Exception as E:
        logging.exception(E)

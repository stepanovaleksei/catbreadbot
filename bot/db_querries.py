import sqlite3


class Queries:
    def __init__(self, database_file: str) -> None:
        """Подключение к БД и сохранение курсора соединения."""
        self.connection = sqlite3.connect(database=database_file)
        self.cursor = self.connection.cursor()

    def add_message(self, user_id: str, message: str) -> None:
        """Adding new message."""
        with self.connection:
            self.cursor.execute(
                "INSERT INTO messages (user_id, message) VALUES (?, ?)",
                (user_id, message),
            )

    def account_exist(self, user_id: str) -> bool:
        """Checking user existence in database."""
        with self.connection:
            result = self.cursor.execute(
                "SELECT * FROM users WHERE user_id =?", (user_id,)
            ).fetchall()
            return bool(result)

    def add_user(self, user_id: str) -> None:
        """Adding new user."""
        with self.connection:
            self.cursor.execute("INSERT INTO users (user_id) VALUES (?)", (user_id,))

    def count_users(self) -> int:
        """Counting all users."""
        with self.connection:
            return len(self.cursor.execute("SELECT * FROM users").fetchall())

    def count_messages(self) -> int:
        """Counting all messages."""
        with self.connection:
            return len(self.cursor.execute("SELECT * FROM messages").fetchall())

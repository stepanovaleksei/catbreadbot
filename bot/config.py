import os

bot_token = os.environ.get("TOKEN")
chat_id = os.environ.get("CHAT_ID")
db_name = os.environ.get("DB_NAME")
